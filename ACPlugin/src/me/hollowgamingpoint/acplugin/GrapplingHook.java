package me.hollowgamingpoint.acplugin;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fish;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerFishEvent.State;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

public class GrapplingHook implements Listener{
	
	Map<String, Boolean> lineIsOut = new HashMap<String, Boolean>();
	Map<String, Fish> hookDest = new HashMap<String, Fish>();
	Map<String, PlayerFishEvent> fishEventMap = new HashMap<String, PlayerFishEvent>();

	@SuppressWarnings("deprecation")
	@EventHandler
	public void throwHook(ProjectileLaunchEvent event) 
	{
		if ((event.getEntity() instanceof Fish))
		{
			Fish hook = (Fish) event.getEntity();
			if ((hook.getShooter() != null)&&((hook.getShooter() instanceof Player)))
			{
				Player player = (Player) hook.getShooter();
				ItemStack iih = new ItemStack(player.getItemInHand());
				ItemMeta im = iih.getItemMeta();
					setLineOut(player, Boolean.valueOf(true));
					this.hookDest.put(player.getName(), hook);
				}
			}
		}

	@EventHandler
	public void fishEvent(PlayerFishEvent event) 
	{
		Player player = event.getPlayer();
		ItemStack iih = new ItemStack(player.getItemInHand());
			if (event.getState() == PlayerFishEvent.State.IN_GROUND || event.getHook().getLocation().getBlock().getRelative(BlockFace.DOWN).getType() != Material.AIR)
			{
				if (this.hookDest.get(player.getName()) == null)
				{
					return;
				}
				Location loc = ((Fish) this.hookDest.get(player.getName())).getLocation();
				loc.setPitch(player.getLocation().getPitch());
				loc.setYaw(player.getLocation().getYaw());
				player.getWorld().playSound(loc, Sound.MAGMACUBE_JUMP, 10.0F, 1.0F);
				pullEntityToLocation(player, loc);
				
			}
			else if(event.getState() == State.CAUGHT_FISH)
			{
				event.setCancelled(true);
			}
			else
			{
				if(event.getState() != State.FISHING)
				{
					event.getPlayer().sendMessage(ChatColor.RED + "Unable to grapple!");
				}
			}
			
			if (event.getState() != PlayerFishEvent.State.FISHING) 
			{
				setLineOut(player, Boolean.valueOf(false));
				setFishEvent(player, null);
			}
			else
			{
				setFishEvent(player, event);
			}
		}
	
	private void pullEntityToLocation(Entity e, Location loc)
	{
		Location entityLoc = e.getLocation();
		entityLoc.setY(entityLoc.getY() + 0.5D);
		e.teleport(entityLoc);
		double g = -0.08D;
		double d = loc.distance(entityLoc);
		double t = d;
		double v_x = (1.0D + 0.07000000000000001D * t) * (loc.getX() - entityLoc.getX()) / t;
		double v_y = (1.0D + 0.03D * t) * (loc.getY() - entityLoc.getY()) / t - 0.5D * g * t;
		double v_z = (1.0D + 0.07000000000000001D * t) * (loc.getZ() - entityLoc.getZ()) / t;
		Vector v = e.getVelocity();
		v.setX(v_x);
		v.setY(v_y);
		v.setZ(v_z);
		e.setVelocity(v);
	}
	
	public void setLineOut(Player player, Boolean b)
	{
		this.lineIsOut.put(player.getName(), b);
	}

	public Boolean getLineOut(Player player) 
	{
		if (this.lineIsOut.containsKey(player.getName())) 
		{
			return (Boolean) this.lineIsOut.get(player.getName());
		}
		return Boolean.valueOf(false);
	}

	public void setFishEvent(Player player, PlayerFishEvent e) 
	{
		this.fishEventMap.put(player.getName(), e);
	}

	public PlayerFishEvent getFishEvent(Player player)
	{
		if (this.fishEventMap.containsKey(player.getName()))
		{
			return (PlayerFishEvent) this.fishEventMap.get(player.getName());
		}
		return null;
	}
	
}
