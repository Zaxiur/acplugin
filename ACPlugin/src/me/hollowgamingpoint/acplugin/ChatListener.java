package me.hollowgamingpoint.acplugin;

import java.util.Arrays;
import java.util.List;

import me.hollowgamingpoint.fanciful.FancyMessage;
import me.hollowgamingpoint.util.ItemUtils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener{
	Main plugin;

	public ChatListener(Main plugin)
	{
		this.plugin = plugin;
	}
	public List<String> Player(Player p)
	  {
		 return Arrays.asList(new String[] { "Level: " + p.getLevel() + "/31",
				                             "Money: " + (int)plugin.economy.getBalance(p.getName()),
				                             "Highest KS: " + plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak")});
	  }
	public List<String> Beginner(Player p)
	  {
		 return Arrays.asList(new String[] { "Required:",
				                             "Highest KS: 1-2"});
	  }
	public List<String> Survivor(Player p)
	  {
		 return Arrays.asList(new String[] { "Required:",
				                             "Highest KS: 3-6"});
	  }
	public List<String> Killer(Player p)
	  {
		 return Arrays.asList(new String[] { "Required:",
				                             "Highest KS: 7-9"});
	  }
	public List<String> Boss(Player p)
	  {
		 return Arrays.asList(new String[] { "Required:",
				                             "Highest KS: 10-14"});
	  }
	public List<String> DeathMachine(Player p)
	  {
		 return Arrays.asList(new String[] { "Required:",
				                             "Highest KS: 15-19"});
	  }
	public List<String> Assassin(Player p)
	  {
		 return Arrays.asList(new String[] { "Required:",
				                             "Highest KS: 20-24"});
	  }
	public List<String> Hunter(Player p)
	  {
		 return Arrays.asList(new String[] { "Required:",
				                             "Highest KS: 25-29"});
	  }
	public List<String> Legendary(Player p)
	  {
		 return Arrays.asList(new String[] { "Required:",
				                             "Highest KS: 30-34"});
	  }
	public List<String> Spartacus(Player p)
	  {
		 return Arrays.asList(new String[] { "Required:",
				                             "Highest KS: 35-39"});
	  }
	public List<String> Ultimate(Player p)
	  {
		 return Arrays.asList(new String[] { "Required:",
				                             "Highest KS: 40-44"});
	  }
	public List<String> HalfImmortal(Player p)
	  {
		 return Arrays.asList(new String[] { "Required:",
				                             "Highest KS: 45-49"});
	  }
	public List<String> Immortal(Player p)
	  {
		 return Arrays.asList(new String[] { "Required:",
				                             "Highest KS: 50-##"});
	  }
	@EventHandler(priority = EventPriority.HIGHEST)
	public void chatcolor(AsyncPlayerChatEvent event) {
		 for(final Player player : Bukkit.getOnlinePlayers()){ 
		int lvl = event.getPlayer().getLevel();
		Player p = event.getPlayer();
		String message = event.getMessage(); 
			if(plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") <= 2 || plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") == 2){
				new FancyMessage(ChatColor.translateAlternateColorCodes('�', "�7[�6Beginner�7] "))
                .itemTooltip(ItemUtils.getStone("�6Beginner", Beginner(p), ChatColor.GRAY))
		    .then(p.getDisplayName())
                .itemTooltip(ItemUtils.getStone("�6Profile: " + p.getName(), Player(p), ChatColor.GRAY))
		    .then(ChatColor.translateAlternateColorCodes('�',  "�6�l> �r" + message))
		    .send(player);
				event.setCancelled(true);
				event.setFormat(ChatColor.translateAlternateColorCodes('�', "�7[�6Beginner�7] " + p.getDisplayName() + " �6�l> �r" + message));
				plugin.userdata.set(p.getUniqueId() + ".rank2", "Beginner");
				plugin.userdata.saveConfig();
			}else
				if(plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") <= 6 || plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") == 3){
					
					new FancyMessage(ChatColor.translateAlternateColorCodes('�', "�7[�6Survivor�7] "))
	                .itemTooltip(ItemUtils.getStone("�6Survivor", Survivor(p), ChatColor.GRAY))
					
			    .then(p.getDisplayName())
                  .itemTooltip(ItemUtils.getStone("�6Profile: " + p.getName(), Player(p), ChatColor.GRAY))
			    .then(ChatColor.translateAlternateColorCodes('�',  "�6�l> �r" + message))
			    .send(player);
					event.setCancelled(true);
					plugin.userdata.set(p.getUniqueId() + ".rank2", "survivor");
					plugin.userdata.saveConfig();
				}else
					if(plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") <= 9 || plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") == 7){
						new FancyMessage(ChatColor.translateAlternateColorCodes('�', "�7[�6Killer�7] "))
		                .itemTooltip(ItemUtils.getStone("�6Killer", Killer(p), ChatColor.GRAY))
				    .then(p.getDisplayName())
                                                        .itemTooltip(ItemUtils.getStone("�6Profile: " + p.getName(), Player(p), ChatColor.GRAY))
				    .then(ChatColor.translateAlternateColorCodes('�',  "�6�l> �r" + message))
				    .send(player);
						event.setCancelled(true);
						plugin.userdata.set(p.getUniqueId() + ".rank2", "killer");
						plugin.userdata.saveConfig();
					}else
						if(plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") <= 14 || plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") == 10){
							new FancyMessage(ChatColor.translateAlternateColorCodes('�', "�7[�6Boss�7] "))
			                .itemTooltip(ItemUtils.getStone("�6Boss", Boss(p), ChatColor.GRAY))
					    .then(p.getDisplayName())
                                                        .itemTooltip(ItemUtils.getStone("�6Profile: " + p.getName(), Player(p), ChatColor.GRAY))
					    .then(ChatColor.translateAlternateColorCodes('�',  "�6�l> �r" + message))
					    .send(player);
							event.setCancelled(true);
							plugin.userdata.set(p.getUniqueId() + ".rank2", "boss");
							plugin.userdata.saveConfig();
						}else
							if(plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") <= 19 || plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") == 15){
								new FancyMessage(ChatColor.translateAlternateColorCodes('�', "�7[�6DeathMachine�7] "))
				                .itemTooltip(ItemUtils.getStone("�6DeathMachine", DeathMachine(p), ChatColor.GRAY))

						    .then(p.getDisplayName())
                                                        .itemTooltip(ItemUtils.getStone("�6Profile: " + p.getName(), Player(p), ChatColor.GRAY))
						    .then(ChatColor.translateAlternateColorCodes('�',  "�6�l> �r" + message))
						    .send(player);
								event.setCancelled(true);
								plugin.userdata.set(p.getUniqueId() + ".rank2", "DeathMachine");
								plugin.userdata.saveConfig();
							}else								
								if(plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") <= 24 || plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") == 20){
									new FancyMessage(ChatColor.translateAlternateColorCodes('�', "�7[�6Assassin�7] "))
					                .itemTooltip(ItemUtils.getStone("�6Assassin", Assassin(p), ChatColor.GRAY))

							    .then(p.getDisplayName())
                                                        .itemTooltip(ItemUtils.getStone("�6Profile: " + p.getName(), Player(p), ChatColor.GRAY))
							    .then(ChatColor.translateAlternateColorCodes('�',  "�6�l> �r" + message))
							    .send(player);
									event.setCancelled(true);
									plugin.userdata.set(p.getUniqueId() + ".rank2", "Ultimate");
									plugin.userdata.saveConfig();
								}else								
									if(plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") <= 29 || plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") == 25){
										new FancyMessage(ChatColor.translateAlternateColorCodes('�', "�7[�6Hunter�7] "))
						                .itemTooltip(ItemUtils.getStone("�6Hunter", Hunter(p), ChatColor.GRAY))
								    .then(p.getDisplayName())
                                                        .itemTooltip(ItemUtils.getStone("�6Profile: " + p.getName(), Player(p), ChatColor.GRAY))
								    .then(ChatColor.translateAlternateColorCodes('�',  "�6�l> �r" + message))
								    .send(player);
										event.setCancelled(true);
										plugin.userdata.set(p.getUniqueId() + ".rank2", "Ultimate");
										plugin.userdata.saveConfig();
									}else								
										if(plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") <= 34 || plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") == 30){
											new FancyMessage(ChatColor.translateAlternateColorCodes('�', "�7[�6Legendary�7] "))
							                .itemTooltip(ItemUtils.getStone("�6Legendary", Legendary(p), ChatColor.GRAY))
									    .then(p.getDisplayName())
                                                        .itemTooltip(ItemUtils.getStone("�6Profile: " + p.getName(), Player(p), ChatColor.GRAY))
									    .then(ChatColor.translateAlternateColorCodes('�',  "�6�l> �r" + message))
									    .send(player);
											event.setCancelled(true);
											plugin.userdata.set(p.getUniqueId() + ".rank2", "Ultimate");
											plugin.userdata.saveConfig();
										}else								
											if(plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") <= 39 || plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") == 35){
												new FancyMessage(ChatColor.translateAlternateColorCodes('�', "�7[�6Spartacus�7] "))
								                .itemTooltip(ItemUtils.getStone("�6Spartacus", Spartacus(p), ChatColor.GRAY))

										    .then(p.getDisplayName())
                                                        .itemTooltip(ItemUtils.getStone("�6Profile: " + p.getName(), Player(p), ChatColor.GRAY))
										    .then(ChatColor.translateAlternateColorCodes('�',  "�6�l> �r" + message))
										    .send(player);
												event.setCancelled(true);
												plugin.userdata.set(p.getUniqueId() + ".rank2", "Ultimate");
												plugin.userdata.saveConfig();
											}else								
												if(plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") <= 44 || plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") == 40){
													new FancyMessage(ChatColor.translateAlternateColorCodes('�', "�7[�6Ultimate�7] "))
									                .itemTooltip(ItemUtils.getStone("�6Ultimate", Ultimate(p), ChatColor.GRAY))

											    .then(p.getDisplayName())
                                                        .itemTooltip(ItemUtils.getStone("�6Profile: " + p.getName(), Player(p), ChatColor.GRAY))
											    .then(ChatColor.translateAlternateColorCodes('�',  "�6�l> �r" + message))
											    .send(player);
													event.setCancelled(true);
													plugin.userdata.set(p.getUniqueId() + ".rank2", "Ultimate");
													plugin.userdata.saveConfig();
												}else								
													if(plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") <= 49 || plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") == 45){
														new FancyMessage(ChatColor.translateAlternateColorCodes('�', "�7[�6Half-Immortal�7] "))
										                .itemTooltip(ItemUtils.getStone("�Half-Immortal", HalfImmortal(p), ChatColor.GRAY))

												    .then(p.getDisplayName())
                                                        .itemTooltip(ItemUtils.getStone("�6Profile: " + p.getName(), Player(p), ChatColor.GRAY))
												    .then(ChatColor.translateAlternateColorCodes('�',  "�6�l> �r" + message))
												    .send(player);
														event.setCancelled(true);
														plugin.userdata.set(p.getUniqueId() + ".rank2", "Ultimate");
														plugin.userdata.saveConfig();
													}else								
														if(plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") <= 49 || plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") == 50 || plugin.userdata.getInt(p.getUniqueId() + ".highestkillstreak") >= 51){
															new FancyMessage(ChatColor.translateAlternateColorCodes('�', "�7[�6Immortal�7] "))
											                .itemTooltip(ItemUtils.getStone("�6Immortal", Immortal(p), ChatColor.GRAY))

													    .then(p.getDisplayName())
                                                        .itemTooltip(ItemUtils.getStone("�6Profile: " + p.getName(), Player(p), ChatColor.GRAY))
													    .then(ChatColor.translateAlternateColorCodes('�',  "�6�l> �r" + message))
													    .send(player);
															event.setCancelled(true);
															plugin.userdata.set(p.getUniqueId() + ".rank2", "Ultimate");
															plugin.userdata.saveConfig();
													}else 
														if(plugin.userdata.getInt(p.getUniqueId() + ".Guard") == 1) {
															new FancyMessage(ChatColor.translateAlternateColorCodes('�', "�7[�bGuard�7] "))
															.tooltip("Guards are here to help you.")
													    .then(p.getDisplayName())
                                                        .itemTooltip(ItemUtils.getStone("�6Profile: " + p.getName(), Player(p), ChatColor.GRAY))
													    .then(ChatColor.translateAlternateColorCodes('�',  "�6�l> �r" + message))
													    .send(player);
															event.setCancelled(true);
							
		}
			
		
}
	}
	}
